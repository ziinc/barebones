module.exports = {
  webdriverio: {
    desiredCapabilities: {
      chromeOptions: {
        args: ["headless", "disable-gpu", "no-sandbox", "disable-extensions"]
      },
      isHeadless: true
    }
  },
  mocha: true,
  mochaConfig: {
    timeout: 120000
  },
  chai: true,
  path: "tests/chimp",
  mochaCommandLineOptions: { bail: true },
  screenshotsOnError: true // saves screenshots to .screenshots folder
};
