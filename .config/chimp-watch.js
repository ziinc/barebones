module.exports = {
  webdriverio: {
    waitforTimeout: 1000
  },
  ddp: "http://localhost:3000",
  watch: true,
  mocha: true,
  chai: true,
  path: "tests/chimp",
  mochaCommandLineOptions: { bail: false },
  mochaConfig: {
    tags: "@watch"
  }
};
