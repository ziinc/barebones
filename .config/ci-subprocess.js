// Inspired from: https://github.com/xolvio/qualityfaster/blob/master/.scripts/
import { exec } from "child_process";

const serverCommand = process.argv[2];

const chimpCommand = process.argv[3];
const serverProcess = exec(serverCommand); // 'npm start'

serverProcess.stdout.pipe(process.stdout);
serverProcess.stderr.pipe(process.stderr);

process.on("exit", code => {
  serverProcess.kill("SIGINT");
});

process.on("uncaughtException", err => {
  process.exit(1);
});

serverProcess.stderr.on("data", data => {
  if (
    data.toString().match("Your application is crashing") ||
    data.toString().match("Exited with code")
  ) {
    serverProcess.kill("SIGINT");
    process.exitCode = code;
    process.exit();
  }
});

serverProcess.stdout.on("data", data => {
  if (
    data.toString().match("Your application is crashing") ||
    data.toString().match("Exited with code")
  ) {
    serverProcess.kill("SIGINT");
    process.exitCode = code;
    process.exit();
  }

  if (data.toString().match("App running at")) {
    const chimpProcess = exec(chimpCommand);
    chimpProcess.stdout.pipe(process.stdout);
    chimpProcess.stderr.pipe(process.stderr);

    chimpProcess.on("close", (code, signal) => {
      serverProcess.kill("SIGINT");
      process.exitCode = code;
      process.exit();
    });
  }
});
