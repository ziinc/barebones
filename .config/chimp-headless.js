module.exports = {
  webdriverio: {
    desiredCapabilities: {
      chromeOptions: {
        args: ["headless", "disable-gpu", "no-sandbox", "disable-extensions"]
      },
      isHeadless: true
    }
  },
  ddp: "http://localhost:3000",
  watch: true,
  mocha: true,
  mochaConfig: {
    timeout: 120000
  },
  chai: true,
  path: "tests/chimp",
  mochaCommandLineOptions: { bail: false },
  screenshotsOnError: true // saves screenshots to .screenshots folder
};
