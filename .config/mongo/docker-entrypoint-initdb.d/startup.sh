mongo --eval 'rs.initiate()'
# mongo --eval 'rs.add("localhost:27017")'
# Sleep for 5 seconds because replicaset takes time to setup.
sleep 5
mongo --eval 'rs.status()'

# if else test: if (db.getUser('meteor')){ print('present')} else {print('not present')}
mongo localhost:27017/meteor --eval 'if (db.getUser("admin")){ print("present, not inserting user")} else {print("not present, inserting user"); db.createUser({user:"admin",pwd:"pass", roles:["readWrite"]}); } '
mongo localhost:27017/meteor --eval 'if (db.getUser("meteor")){ print("present, not inserting user")} else {print("not present, inserting user"); db.createUser({user:"meteor",pwd:"pass", roles:["readWrite"]}); } '
mongo localhost:27017/admin --eval 'if (db.getUser("meteor-oplog")){ print("present, not inserting user")} else {print("not present, inserting user"); db.createUser({user:"meteor-oplog",pwd:"pass", roles:[{role: "read", db: "local"}]}); } '