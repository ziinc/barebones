const { version } = require("../package");
module.exports = {
  components: "../src/client/atoms/**/[A-Z]*.js",
  version,
  resolver: require("react-docgen").resolver.findAllComponentDefinitions,
  theme: {
    baseBackground: "#fdfdfc",
    link: "#274e75",
    linkHover: "#90a7bf",
    border: "#e0d2de",
    font: ["Helvetica", "sans-serif"]
  },
  styles: {
    Playground: {
      preview: {
        paddingLeft: 0,
        paddingRight: 0,
        borderWidth: [[0, 0, 1, 0]],
        borderRadius: 0
      }
    },
    Markdown: {
      pre: {
        border: 0,
        background: "none"
      },
      code: {
        fontSize: 14
      }
    }
  },
  showCode: true,
  require: ["bootstrap/dist/css/bootstrap.css"],
  webpackConfig: {
    module: {
      rules: [
        // Babel loader, will use your project’s .babelrc
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: [
              "@babel/plugin-proposal-object-rest-spread",
              "@babel/plugin-proposal-class-properties"
            ]
          }
        },
        {
          test: /\.svg$/,
          loader: "url-loader"
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        }
      ]
    }
  }
};
