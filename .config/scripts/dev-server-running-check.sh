#!/bin/bash
host="$1"
shift
cmd="$@"
# curl -sSL http://"$host":3000
until ! curl -sSL http://"$host":3000 2>&1 | grep -q 'Connection refused' ; do
  echo $?
  sleep 2
done
echo "Server is up - executing command"
exec "$@"