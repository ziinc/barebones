FROM registry.gitlab.com/ziinc/barebones/meteor-base:latest as build

ARG USERNAME=dev-user
RUN useradd -m $USERNAME

COPY . /opt/code
WORKDIR /opt/code
RUN chown -Rh $USERNAME /opt/code

# Dependencies needed for npm install
# includes: node-gyp dependencies
RUN apt-get -y update && \
    apt-get -y --no-install-recommends install \
    build-essential \
    g++ \
    git \
    python \
    ssh && \
    rm -rf /var/lib/apt/lists/*

# create dist directory
RUN mkdir -p /opt/dist \
    && chown -Rh $USERNAME /opt/dist

# switch tar to bsdtar as root
# https://github.com/meteor/meteor/issues/5762#issuecomment-327854093
USER root
RUN echo -n "tar version:"; tar --version \
    && mv /bin/tar /bin/tar.gnu \
    && ln -s /usr/bin/bsdtar /bin/tar \
    && echo -n "tar version:"; tar --version
USER $USERNAME
RUN if  ! meteor npm install --production ; \
    then rm -rf node_modules && echo 'Disabling strict ssl' && meteor npm config set strict-ssl false &&  meteor npm install --production; \
    fi

# Create deployment bundle
RUN meteor build --server-only --architecture os.linux.x86_64 /opt/dist 

# # Put the original tar back as root
USER root
RUN mv /bin/tar.gnu /bin/tar

FROM node:8.11.3-alpine
COPY --from=build /opt/dist/code.tar.gz /opt/dist/code.tar.gz
# RUN apk update && apk add adduser

ARG USERNAME=app-user
RUN adduser -D $USERNAME \
    &&  mkdir -p /opt/app  \
    && chown -Rh $USERNAME /opt/app
RUN tar -xf /opt/dist/code.tar.gz -C /opt/app
WORKDIR /opt/app/bundle

RUN rm -rf /opt/dist

RUN ls -l /opt/app/bundle
WORKDIR /opt/app/bundle/programs/server

RUN apk update && apk add python make g++ \
    && if  ! npm install --production ; \
    then rm -rf node_modules && echo 'Disabling strict ssl' && npm config set strict-ssl false &&  npm install --production && npm config set strict-ssl true; \
    fi \
    && apk del python make g++

WORKDIR /opt/app/bundle
USER $USERNAME
CMD ["node", "main.js"]