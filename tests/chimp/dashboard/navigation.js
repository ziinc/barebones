import {
  baseUrl,
  signups,
  loginPath,
  dashboardPath
} from "../../../src/App-settings.test.js";
import { loginFakeUser0, resetDb, insertFakeUser0 } from "../helpers";

describe("Dashboard Navigation Tests @watch", function() {
  beforeEach(function() {
    browser.url(baseUrl);
  });
  afterEach(() => {
    resetDb();
  });
  describe("As a registered user", function() {
    describe("when logged-in onto the dashboard home page", function() {
      beforeEach(function() {
        insertFakeUser0();
        loginFakeUser0();
        browser.url(`${baseUrl}${dashboardPath}`);
        browser.waitForExist("#dashboard");
      });
      it("dashboard --> help page");
      it("dashboard --> about page");
      it("dashboard --> legal page");
      it("dashboard --> settings page");
    });
  });
});
