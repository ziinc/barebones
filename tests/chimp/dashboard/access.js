import {
  baseUrl,
  signups,
  loginPath,
  dashboardPath
} from "../../../src/App-settings.test.js";
import { loginFakeUser0, resetDb, insertFakeUser0 } from "../helpers";

describe("Dashboard Access Tests @watch", function() {
  beforeEach(function() {
    browser.url(baseUrl);
  });
  afterEach(() => {
    resetDb();
  });
  describe("As a visitor", function() {
    it("should have no access to the dashboard", function() {
      // navigate to dashboard
      browser.url(`${baseUrl}${dashboardPath}`);

      // should be redirected to login, should see login forms instead of dashboard
      browser.waitForExist("#login-form");
      expect(browser.isExisting("#dashboard"), "dashboard does not exist").to.be
        .false;
      expect(browser.isVisible("#login-form"), "able to see login form").to.be
        .true;
      expect(browser.getUrl()).to.not.contain(dashboardPath);

      // fully implemented
    });
  });
  describe("As a registered user", function() {
    describe("when logged-in", function() {
      beforeEach(function() {
        insertFakeUser0();
        loginFakeUser0();
      });
      it("should be able to access dashboard through direct path", function() {
        // navigate to dashboard
        browser.url(`${baseUrl}${dashboardPath}`);
        browser.waitForExist("#dashboard");

        // should show the correct url
        expect(browser.getUrl()).to.contain(dashboardPath);

        // fully implemented
      });
      it("should be redirected to dashboard when entering website root home", function() {
        // navigate to root url, /
        browser.url(baseUrl);
        browser.pause(200);

        // should be redirected to the dashboard
        expect(browser.getUrl()).to.contain(dashboardPath);
        browser.waitForExist("#dashboard");

        // fully implemented
      });
    });
  });
});
