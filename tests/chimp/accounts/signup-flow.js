import { baseUrl, signups, loginPath } from "../../../src/App-settings.test.js";
import { resetDb } from "../helpers/index.js";
describe("Signup Flow Tests", function() {
  beforeEach(() => {
    browser.url(baseUrl);
  });
  afterEach(function() {
    resetDb();
  });
  describe("As an unregistered user ", () => {
    if (signups) {
      // signups are enabled
      it("should be able to signup from loginpage", function() {
        browser.url(`${baseUrl}${loginPath}`);
        browser.waitForExist("#login-form");
        browser.click("#form-switcher");
        browser.waitForExist("#signup-form");
        browser.waitForExist("*=Sign Up");
        // fully implemented
      });
    } else {
      // signups are not enabled
      it("should not be able to see signup form elements at login page", function() {
        browser.url(`${baseUrl}${loginPath}`);
        browser.waitForExist("#login-form");
        expect(browser.isExisting("#signup-form")).to.be.false;
        expect(browser.isExisting("*=Sign Up")).to.be.false;
        expect(browser.isExisting("#form-switcher")).to.be.false;
        // fully implemented
      });
    }
  });
});
