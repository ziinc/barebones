import {
  baseUrl,
  enrollmentRequests,
  loginPath
} from "../../../src/App-settings.test.js";

describe("Accounts Enrollment Flow Tests", function() {
  describe("As an unregistered user", function() {
    beforeEach(function() {
      browser.url(baseUrl);
    });
    if (enrollmentRequests) {
      // enrollment requests enabled
      it("should be able to request for enrollment", function() {
        // assume login path allows for enrollment
        browser.url(`${baseUrl}${loginPath}`);
        expect(browser.isExisting("#request-enrollment")).to.be.true;

        // should see enrollment button.
        // should be able to request for enrollment.
        // Email and other information to be provided.
        // should navigate to enrollment link
        // should set password
        // should be redirected to dashboard
        throw new Error("not implemented");
      });
    } else {
      // enrollment requests not enabled
      it("should not be able to request for enrollment", function() {
        // assume login path allows for enrollment
        browser.url(`${baseUrl}${loginPath}`);
        // should not see enrollment button
        expect(browser.isExisting("#request-enrollment")).to.be.false;

        throw new Error("not implemented");
      });
    }
  });
});
