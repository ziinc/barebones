import { baseUrl, rolesConfigAbsPath } from "../../../src/App-settings.test.js";
import {
  loginFakeUser0,
  insertFakeUser0,
  removeFakeUsers
} from "../helpers/index.js";

describe("Roles tests", function() {
  describe("As a normal customer...", function() {
    beforeEach(function() {
      browser.url(baseUrl);
      removeFakeUsers();
      insertFakeUser0({
        group: "customer",
        subGroup: "normal"
      });
      loginFakeUser0();
    });
    afterEach(function() {
      removeFakeUsers();
    });
    it("should have the correct number of permissions", function() {
      const obj = browser.execute(function() {
        return Meteor.userId();
      });
      const userId = obj.value;
      const roles = server.execute(function(userId) {
        const Roles = require("meteor/alanning:roles").Roles;
        const Meteor = require("meteor/meteor").Meteor;
        const result = Roles.getRolesForUser(userId, "customer");
        return result;
      }, userId);
      const correctRolesCount = server.execute(function(rolesConfigAbsPath) {
        const roleGroups = require(rolesConfigAbsPath).roleGroups;
        return roleGroups.customer.normal().length;
      }, rolesConfigAbsPath);
      expect(roles.length).to.equal(correctRolesCount);
    });
    it("should not have admin permissions", function() {
      const obj = browser.execute(function() {
        return Meteor.userId();
      });
      const userId = obj.value;
      const roles = server.execute(function(userId) {
        const Roles = require("meteor/alanning:roles").Roles;
        const Meteor = require("meteor/meteor").Meteor;
        const result = Roles.getRolesForUser(userId, "admin");
        return result;
      }, userId);
      expect(roles.length).to.equal(0);
    });
  });
  describe("As a superAdmin user...", function() {
    beforeEach(function() {
      browser.url(baseUrl);
      removeFakeUsers();
      insertFakeUser0({
        group: "admin",
        subGroup: "superAdmin"
      });
      loginFakeUser0();
    });
    afterEach(function() {
      removeFakeUsers();
    });
    it("should have the correct number of permissions", function() {
      const obj = browser.execute(function() {
        return Meteor.userId();
      });
      const userId = obj.value;

      const roles = server.execute(function(userId) {
        const Roles = require("meteor/alanning:roles").Roles;
        const Meteor = require("meteor/meteor").Meteor;
        const result = Roles.getRolesForUser(userId, "admin");
        return result;
      }, userId);

      const correctRolesCount = server.execute(function(rolesConfigAbsPath) {
        const roleGroups = require(rolesConfigAbsPath).roleGroups;
        return roleGroups.admin.superAdmin().length;
      }, rolesConfigAbsPath);

      expect(roles.length).to.equal(correctRolesCount);
    });
    it("should have superAdmin admin permissions", function() {
      const obj = browser.execute(function() {
        return Meteor.userId();
      });
      const userId = obj.value;

      const roles = server.execute(function(userId) {
        const Roles = require("meteor/alanning:roles").Roles;
        const Meteor = require("meteor/meteor").Meteor;
        const result = Roles.getRolesForUser(userId, "admin");
        return result;
      }, userId);

      const correctRolesCount = server.execute(function(rolesConfigAbsPath) {
        const roleGroups = require(rolesConfigAbsPath).roleGroups;

        const result = roleGroups.admin.superAdmin().length;
        return result;
      }, rolesConfigAbsPath);
      expect(roles.length).to.equal(correctRolesCount);
    });
  });
});
