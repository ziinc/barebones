import {
  baseUrl,
  signups,
  loginPath,
  loginMethod,
  dashboardPath
} from "../../../src/App-settings.test.js";
import { fakeData, insertFakeUser0, resetDb } from "../helpers/index.js";

describe("Accounts Login Flow Tests", function() {
  beforeEach(function() {
    browser.url(baseUrl);
  });
  afterEach(function() {
    resetDb();
  });
  describe("As an unregistered user", () => {
    describe("when at the login page", function() {
      beforeEach(function() {
        browser.url(`${baseUrl}${loginPath}`);
      });
      it("should be able to see login form elements at login page", function() {
        const loginForm = browser.element("#login-form");
        loginForm.waitForExist("[name='username']");
        loginForm.waitForExist("[name='password']");

        // should see login button

        throw new Error("not implemented");
      });
      it("should be unable to login at loginpage", function() {
        // fill up form with random user password

        // should show login failure popup

        throw new Error("not implemented");
      });
    });
  });

  describe("As a registered user with an account", () => {
    describe("when at the login page", function() {
      beforeEach(function() {
        insertFakeUser0();
        browser.url(`${baseUrl}${loginPath}`);
      });

      it("should be able to see login form elements at login path", function() {
        const loginForm = browser.element("#login-form");
        loginForm.waitForExist("[name='username']");
        loginForm.waitForExist("[name='password']");
        // should see login button

        throw new Error("not implemented");
      });
      it("should be able to login at login path", function() {
        // fill up form
        const loginForm = browser.element("#login-form");
        if (loginMethod == "email") {
          loginForm.setValue("[name='email']", fakeData.user0.email);
        }
        loginForm.setValue("[name='password']", fakeData.user0.password);
        browser.click("#login-btn");
        browser.pause(500);

        // should not show failed login message
        expect(browser.isExisting("#failed-login")).to.be.false;

        // should be redirect to dashboard
        expect(browser.getUrl()).to.contain(dashboardPath);

        // user should be logged in on the browser
        const userId = browser.execute(() => {
          return Meteor.userId();
        }).value;
        expect(userId).to.not.equal(null);

        // fully implemented
      });
      it("should not be able to login with incorrect password", () => {
        // fill up form
        const loginForm = browser.element("#login-form");
        if (loginMethod == "email") {
          loginForm.setValue("[name='email']", fakeData.user0.email);
        }
        loginForm.setValue("[name='password']", "1231231231231");
        browser.click("#login-btn");
        browser.pause(500);

        // should fail login
        expect(browser.isExisting("#failed-login")).to.be.true;
        expect(browser.getUrl()).to.not.contain(dashboardPath);
        expect(browser.getUrl()).to.contain(loginPath);

        // user should not be logged in on browser
        const userId = browser.execute(() => {
          return Meteor.userId();
        }).value;
        expect(userId).to.equal(null);

        // fully implemented
      });
    });
  });
});
