import { baseUrl, signups, loginPath } from "../../../src/App-settings.test.js";
import { resetDb } from "../helpers/index.js";

describe("Reset Password Tests", function() {
  beforeEach(() => {
    browser.url(baseUrl);
  });
  afterEach(function() {
    resetDb();
  });
  describe("As a registered user", () => {
    describe("when at the login page", function() {
      beforeEach(function() {
        browser.url(`${baseUrl}${loginPath}`);
        // insert fake user
        throw new Error("not implemented");
      });
      it("should be able to request for password reset from", function() {
        browser.waitForExist("#request-password-reset");

        // should see password request form

        // should fill up password request form

        // should click submit password request form

        // should show password request success message

        // ...

        throw new Error("not implemented");
      });
    });
  });
});
