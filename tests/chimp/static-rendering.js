import { baseUrl } from "../../src/App-settings.test.js";
describe("static html rendering", function() {
  beforeEach(function() {
    browser.url(baseUrl);
  });

  it("main.html has correct server and client render target", function() {
    console.log("test");
    expect(browser.isExisting("#client-render-target")).to.be.true;
    expect(browser.isExisting("#server-render-target")).to.be.true;
  });
});
