import { rolesConfigAbsPath } from "../../../src/App-settings.test.js";
export const fakeData = {
  user0: {
    username: "infra-tester",
    first_name: "infra-0",
    last_name: "tester-0",
    email: "abc0@example123123123.com",
    password: "123456789"
  },
  user1: {
    username: "infra-tester-1",
    first_name: "infra-1",
    last_name: "tester-1",
    email: "abc1@example123123123.com",
    password: "123456789"
  }
};
export const resetDb = () => {
  server.execute(() => {
    const resetDatabase = require("meteor/xolvio:cleaner").resetDatabase;
    resetDatabase();
  });
  browser.execute(() => {
    const resetDatabase = require("meteor/xolvio:cleaner").resetDatabase;
    resetDatabase();
  });
};
export const fetchConsoleLogs = () => {
  // console log only last 3 logs
  const logs = browser.log("browser");
  if (logs.value && logs.value.length > 0) {
    const logsArr = logs.value;
    logsArr.forEach(logObj => {
      const allowedLevels = ["SEVERE"];
      const message = `[${logObj.level}] ${logObj.message}`;
      if (allowedLevels.includes(logObj.level)) {
        // spacing between each msg
        console.log("");
        let splitMsg = message.split(/\\n/g);
        splitMsg.forEach(msg => console.log(msg));
      }
    });
  }
};
export const removeFakeUsers = () => {
  browser.execute(function() {
    Meteor.logout();
  });
  server.execute(function(fakeData) {
    const Meteor = require("meteor/meteor").Meteor;
    Meteor.users.remove({
      emails: { $elemMatch: { address: fakeData.user0.email } }
    });
    Meteor.users.remove({
      emails: { $elemMatch: { address: fakeData.user1.email } }
    });
  }, fakeData);
};

/**
 * inserts a fake user
 * @param {object} role an object specifying the role of the user
 * @param {String} role.group either customer or admin
 * @param {String} role.subGroup the subgroup specified in roles-config
 * @returns {String} the userId
 */
export const insertFakeUser0 = (
  role = { group: "customer", subGroup: "normal" }
) => {
  const username = fakeData.user0.username;
  const email = fakeData.user0.email;
  const password = fakeData.user0.password;
  const userId = server.call("users.onUserSignup", {
    username,
    email,
    password
  });
  if (role) {
    server.execute(
      function(userId, role, rolesConfigAbsPath) {
        const addRolesToUser = require(rolesConfigAbsPath).addRolesToUser;
        addRolesToUser({
          userId,
          group: role.group,
          subGroup: role.subGroup
        });
      },
      userId,
      role,
      rolesConfigAbsPath
    );
  }
  return userId;
};

/**
 * inserts a fake user
 * @param {object} role an object specifying the role of the user
 * @param {String} role.group either customer or admin
 * @param {String} role.subGroup the subgroup specified in roles-config
 * @returns {String} the userId
 */
export const insertFakeUser1 = role => {
  const username = fakeData.user1.username;
  const email = fakeData.user1.email;
  const password = fakeData.user1.password;
  const userId = server.call("users.onUserSignup", {
    username,
    email,
    password
  });
  if (role) {
    server.execute(
      function(userId, role, rolesConfigAbsPath) {
        const addRolesToUser = require(rolesConfigAbsPath).addRolesToUser;
        addRolesToUser({
          userId,
          group: role.group,
          subGroup: role.subGroup
        });
      },
      userId,
      role,
      rolesConfigAbsPath
    );
  }
  return userId;
};

export const findFakeUserId0 = () => {
  const username = fakeData.user0.username;
  const email = fakeData.user0.email;
  const password = fakeData.user0.password;
  const userId = server.execute(function(fakeData) {
    const Meteor = require("meteor/meteor").Meteor;
    return Meteor.users.findOne(
      { username: fakeData.user0.username },
      { fields: { _id: 1 } }
    );
  }, fakeData)._id;
  return userId;
};

/**
 * logs in using user0 details
 */
export const loginFakeUser0 = () => {
  // remove ddp rate limit
  server.execute(function() {
    const Accounts = require("meteor/accounts-base").Accounts;
    Accounts.removeDefaultRateLimit();
  });
  browser.execute(function(fakeData) {
    const email = fakeData.user0.email;
    const password = fakeData.user0.password;
    Meteor.loginWithPassword({ email }, password, (err, res) =>
      console.log(err)
    );
  }, fakeData);
  browser.pause(700);
};

/**
 * logs in using user1 details
 */
export const loginFakeUser1 = () => {
  server.execute(function() {
    const Accounts = require("meteor/accounts-base").Accounts;
    Accounts.removeDefaultRateLimit();
  });
  browser.execute(function(fakeData) {
    const email = fakeData.user1.email;
    const password = fakeData.user1.password;
    Meteor.loginWithPassword({ email }, password, (err, res) =>
      console.log(err)
    );
  }, fakeData);
  browser.pause(700);
};

export const logoutFakeUser = () => {
  browser.execute(function() {
    Meteor.logout();
  });
};

export const userExists = () => {
  const getUser0 = server.execute(function(fakeData) {
    const Accounts = require("meteor/accounts-base").Accounts;
    return Accounts.findUserByEmail(fakeData.user0.email);
  }, fakeData);
  const getUser1 = server.execute(function(fakeData) {
    const Accounts = require("meteor/accounts-base").Accounts;
    return Accounts.findUserByEmail(fakeData.user1.email);
  }, fakeData);
  return Boolean(getUser0) || Boolean(getUser1);
};

/**
 * fills signup form using user0 details
 */
export const fillSignupForm = () => {
  const username = fakeData.user0.username;
  const email = fakeData.user0.email;
  const password = fakeData.user0.password;
  browser
    .setValue('[name="email"]', email)
    .setValue('[name="username"]', username)
    .setValue('[name="password"]', password)
    .setValue('[name="retype-password"]', password);
};
export const fillSignupForm1 = () => {
  const username = fakeData.user1.username;
  const email = fakeData.user1.email;
  const password = fakeData.user1.password;
  browser
    .setValue('[name="email"]', email)
    .setValue('[name="username"]', username)
    .setValue('[name="password"]', password)
    .setValue('[name="retype-password"]', password);
};

/**
 * fills login form using user0 details
 */
export const fillLoginForm = () => {
  const email = fakeData.user0.email;
  const password = fakeData.user0.password;
  browser
    .setValue('[name="email"]', email)
    .setValue('[name="password"]', password);
};
