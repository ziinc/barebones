# Barebones Meteor

Goals of this project:

- To provide a **simple**, yet **comprehensive** template for the entire meteor application development/testing/deployment process.

## Running Tests

```
# runs chimp in watch mode
npm run chimp

# runs chimp in headless watch mode
npm run chimp-headless

# runs chimp in headless in all tests mode
npm run chimp-ci

# runs chimp in browser in all tests mode
npm run chimp-all
```

## Design Choices

### Atomic Design for React

Atoms = building blocks

- cannot be nested

Moleculese = templates of building blocks

- cannot be nested

Ecosystems = holds the view elements.

- cannot be nested

Fetchers = fetches data

- HOC pattern
- Adds props to ecosystems
- overrides props according to order of initialization

```javascript
import React, { Component } from "react";
// import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

export function fetchSomeData(WrappedComponent, tracker = false) {
  if (tracker) {
    return withTracker(props => {
      // subscribe to publications
      // call some data
      return {
        // some data
        ...props
      };
    })(WrappedComponent);
  } else {
    return class extends Component {
      constructor(props) {
        super(props);
        this.state = {
          // add keys
        };
      }
      componentDidMount() {
        // fetch some data with a Meteor call or a http fetch
      }
      render() {
        const data = {
          ...this.state,
          ...this.props
        };
        return <WrappedComponent {...data} />;
      }
    };
  }
}
```

Environment = figures out what ecosystems should be viewed and what data should be fetched

- file contains ecosystems
