export const baseUrl = process.env.ROOT_URL || "http://localhost:3000";

export const loginPath = "/"; // either '/' or '/login'
export const loginMethod = "both"; // either 'email', 'username', or both
export const signups = true; // if true, attempt to signup from loginPath.
export const enrollments = true; // if true, user account is created through enrollment
export const resetPassword = true; // if true, user account passwords can be reset independently

export const dashboardPath = "/dashboard";
export const enrollmentRequests = false;

export const rolesConfigAbsPath = "/src/api/accounts/server/roles.js";
