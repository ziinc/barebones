import { Meteor } from "meteor/meteor";
import { ValidatedMethod } from "meteor/mdg:validated-method";
import SimpleSchema from "simpl-schema";

import { Accounts } from "meteor/accounts-base";

/**
 * creates a new user
 * @param {string} email the user's enail
 * @param {string} password the user's password
 * @returns {string} the user Id
 */
export const onUserSignup = new ValidatedMethod({
  name: "users.onUserSignup",
  validate: new SimpleSchema({
    username: { type: String },
    email: { type: String },
    password: { type: String }
  }).validator(),
  //add custom validation
  run({ username, email, password }) {
    const userId = Accounts.createUser({
      username,
      email,
      password
    });
    return userId;
  }
});

export const sendVerificationEmail = new ValidatedMethod({
  name: "users.sendVerificationEmail",
  validate: new SimpleSchema({
    userId: { type: String }
  }).validator(),
  run({ userId }) {
    if (Meteor.isServer) {
      const user = Meteor.users.findOne({ _id: userId });
      const email = user.emails[0];

      const emailsToExclude = [];
      if (!emailsToExclude.includes(email.address)) {
        // const { token, url } = Accounts.sendVerificationEmail(userId);
        return { token, url };
      }
    }
  }
});

export const findUserIdByEmail = new ValidatedMethod({
  name: "users.findUserIdByEmail",
  validate: new SimpleSchema({
    email: { type: String }
  }).validator(),
  run({ email }) {
    if (Meteor.isServer) {
      const user = Accounts.findUserByEmail(email);
      if (user === null) {
        throw Meteor.Error("no-user-found", "No user with this email exists");
      }
      return user._id;
    }
  }
});
