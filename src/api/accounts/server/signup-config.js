import { Accounts } from "meteor/accounts-base";

import SimpleSchema from "simpl-schema";

//function runs each time a user is created.
Accounts.onCreateUser((options, user) => {
  return user;
});

Accounts.config({
  sendVerificationEmail: false,
  passwordResetTokenExpirationInDays: 1,
  forbidClientAccountCreation: true
});
