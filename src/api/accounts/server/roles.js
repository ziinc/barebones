import { Meteor } from "meteor/meteor";
import SimpleSchema from "simpl-schema";
import { Roles } from "meteor/alanning:roles";
import { Accounts } from "meteor/accounts-base";
import { Random } from "meteor/random";

/**
 * roleGroups defines the the user groups that will be using the application.
 * It is a JS object, where each key represents a group of users.
 * Within each group are different subgroups (for segmentation).
 * E.g. within the customer group, there are paying customers and non-paying customers.
 * In this case, this would mean having a group called *customer* and 2 subgroups of *paying* and * free*.
 *
 * Each subgroup is represented by a function that returns an array of permissions for that subgroup.
 *
 * If the permissions of each group are hierarchical, it can be achieved through spread notation.
 * E.g. if paying users have more permissions in addition to those of free users, the paying user subgroup
 * would return `return [....this.free(), "1st-permission", "2nd-permission"]`
 */
export const roleGroups = {
  // for the customer group
  customer: {
    normal() {
      return [];
    }
    // intermediate() {
    //   return [...this.beginner(), "private-content"];
    // },
    // advanced() {
    //   return [...this.intermediate(), "premium-content"];
    // }
  },
  admin: {
    superAdmin() {
      return [...this.normalAdmin(), "super-admin"];
    },
    normalAdmin() {
      return ["admin-access"];
    }
  }
};

/**
 * This function adds roles of a subGroup to a user. It is only called by trusted code
 * @param {String} userId
 * @param {String} group role group
 * @param {String} subGroup role subgroup
 */
export const addRolesToUser = ({ userId, group, subGroup }) => {
  // check types
  new SimpleSchema({
    userId: { type: String },
    group: { type: String },
    subGroup: { type: String }
  }).validate({
    userId,
    group,
    subGroup
  });

  if (group == "customer") {
    // validation goes here
  }
  if (group == "admin") {
    // Roles.addUsersToRoles(userId, roleGroups.customer.advanced(), "customer");
  }
  // add roles according to the subgroup they are in.
  Roles.addUsersToRoles(userId, roleGroups[group][subGroup](), group);
};

/**
 * This function assigns admin roles to users identified in the meteor settings file.
 */
export const assignAdminRoles = () => {
  const { superAdmin, normalAdmin } = Meteor.settings.admin.allowedEmails;

  // check Meteor.settings.admin.allowedEmails structure.
  new SimpleSchema({
    email: { type: String },
    username: { type: String }
  }).validate({
    userId,
    group,
    subGroup
  });

  const enrollAdmin = (email, username) => {
    const userId = Accounts.createUser({
      username,
      email,
      password: Random.id(10)
    });
    Accounts.sendEnrollmentEmail(userId);
    return Accounts.findUserById(userId);
  };

  superAdmin.forEach(({ email, username }) => {
    // check if user exists. If not,addRolesToUser then enroll the user.
    let user = Accounts.findUserByEmail(email);
    if (!user) {
      user = enrollAdmin(email, username);
    }
    addRolesToUser({
      userId: user._id,
      group: "admin",
      subGroup: "superAdmin"
    });
  });

  normalAdmin.forEach(({ email, username }) => {
    // check if user exists. If not, then enroll the user.
    const user = Accounts.findUserByEmail(email);
    if (!user) {
      user = enrollAdmin(email, username);
    }
    addRolesToUser({
      userId: user._id,
      group: "admin",
      subGroup: "normalAdmin"
    });
  });
};
