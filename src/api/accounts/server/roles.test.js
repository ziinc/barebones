import { expect } from "chai";
import { roleGroups, addRolesToUser, assignAdminRoles } from "./roles.js";
import { resetDatabase } from "meteor/xolvio:cleaner";
import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";

describe("src/api/accounts/server/roles.js", () => {
  describe("roleGroups config object", () => {
    it("should have group keys", () => {
      const groupKeys = Object.keys(roleGroups);
      expect(groupKeys.length).to.not.equal(0);
    });
    it("should have subGroup keys", () => {
      const groupKeys = Object.keys(roleGroups);
      groupKeys.forEach(groupKey => {
        const subGroups = roleGroups[groupKey];
        const subGroupKeys = Object.keys(subGroups);
        expect(subGroupKeys.length).to.not.equal(0);
      });
    });
    it("each subgroup should an array of permissions (can be zero)", () => {
      const groupKeys = Object.keys(roleGroups);
      groupKeys.forEach(groupKey => {
        const subGroups = roleGroups[groupKey];
        const subGroupKeys = Object.keys(subGroups);
        subGroupKeys.forEach(key => {
          const permissions = subGroups[key]();
          expect(Array.isArray(permissions)).to.be.true;
        });
      });
    });
  });
  //
  //
  describe("addRolesToUser function", () => {
    beforeEach(() => {
      const userId = Accounts.createUser({
        username: "testing123",
        email: "testing@testing.com",
        password: "password"
      });
    });
    afterEach(() => {
      resetDatabase();
    });
    it("should add specified roles to the user.", () => {
      let user = Meteor.users.findOne({ username: "testing123" });
      expect(user._id).to.be.string;
      const userId = user._id;

      const firstGroupKey = Object.keys(roleGroups)[0];
      const firstSubGroupKey = Object.keys(roleGroups[firstGroupKey])[0];

      addRolesToUser({
        userId,
        group: firstGroupKey,
        subGroup: firstSubGroupKey
      });
      user = Meteor.users.findOne(
        { username: "testing123" },
        { fields: { roles: 1 } }
      );
      const groupNames = Object.keys(user.roles);
      const groupName = groupNames[0];
      expect(groupNames.length).to.equal(1);
      expect(groupNames[0]).to.equal(firstGroupKey);
      const permissions = roleGroups[firstGroupKey][firstSubGroupKey]();
      expect(user.roles[groupName]).to.deep.equal(permissions);
    });
  });
  //
  //
  describe("assignAdminRoles function", () => {
    it("should assign admin roles to the user.");
  });
});
