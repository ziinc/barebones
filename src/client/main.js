import { Meteor } from "meteor/meteor";
import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App.js";

import "bootstrap/dist/css/bootstrap.css"; //import css, comment out if using a custom theme.

ReactDOM.render(<App />, document.getElementById("client-render-target"));
