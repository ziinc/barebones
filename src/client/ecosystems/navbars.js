import React from "react";
import { Meteor } from "meteor/meteor";
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import { Link } from "react-router-dom";
/**
 * This file contains different navbars
 */
class BaseNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const { id, children, ...rest } = this.props;
    return (
      <Navbar id={id} {...rest} color="light" light expand="md">
        <NavbarBrand tag={Link} to="/">
          Barebones
        </NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          {children}
        </Collapse>
      </Navbar>
    );
  }
}

export const PublicNavbar = props => {
  return (
    <BaseNavbar id="public-navbar">
      <Nav className="ml-auto" navbar>
        <NavItem>
          <NavLink tag={Link} to="/help">
            Help
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to="/">
            Login
          </NavLink>
        </NavItem>
      </Nav>
    </BaseNavbar>
  );
};
export const DashboardNavbar = props => {
  return (
    <BaseNavbar id="dashboard-navbar">
      <Nav className="ml-auto" navbar>
        <NavItem>
          <NavLink tag={Link} to="/dashboard">
            Dashboard
          </NavLink>
        </NavItem>
      </Nav>
    </BaseNavbar>
  );
};
