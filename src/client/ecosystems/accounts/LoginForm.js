import { Meteor } from "meteor/meteor";
import React, { Component } from "react";
import { PrimaryBtn } from "../../atoms";
import { Link, Redirect } from "react-router-dom";
import { Input, Alert } from "reactstrap";
export class LoginForm extends Component {
  state = {
    email: "",
    password: "",
    redirect: false,
    redirectPath: this.props.redirectPath || "/dashboard",
    showFailedLoginAlert: false
  };
  loginBtnClick(e) {
    e.preventDefault();
    const { email, password } = this.state;
    Meteor.loginWithPassword({ email }, password, (err, res) => {
      if (err) {
        console.log(err);
        this.setState({ showFailedLoginAlert: true });
      } else {
        // redirect
        this.setState({ redirect: true });
      }
    });
  }
  emailChange(e) {
    this.setState({ email: e.target.value, showFailedLoginAlert: false });
  }
  passwordChange(e) {
    this.setState({ password: e.target.value, showFailedLoginAlert: false });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirectPath} />;
    }

    return (
      <div>
        {this.state.showFailedLoginAlert ? (
          <Alert color="warning" id="failed-login">
            <strong>Login Failed!</strong>
            <p>
              Something went wrong. Pleaes check your email and password and try
              again.
            </p>
          </Alert>
        ) : null}

        <form id="login-form" onSubmit={this.loginBtnClick.bind(this)}>
          Login here!
          <Input
            placeholder="Email"
            name="email"
            type="email"
            onChange={e => this.emailChange.bind(this)(e)}
          />
          <Input
            placeholder="Password"
            name="password"
            type="password"
            onChange={e => this.passwordChange.bind(this)(e)}
          />
          <PrimaryBtn type="submit" id="login-btn">
            Login
          </PrimaryBtn>
          {/* <div id="request-reset-btn">Forgot your password?</div> */}
        </form>
      </div>
    );
  }
}
