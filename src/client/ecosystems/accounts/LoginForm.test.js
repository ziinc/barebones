import React from "react";
import { expect } from "chai";
import Enzyme, { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { spy } from "sinon";
import { LoginForm } from "./LoginForm.js";
import { mountWrap, shallowWrap } from "../../helpers/test-helpers.test.js";
Enzyme.configure({ adapter: new Adapter() });

describe("ecosystems/accounts/LoginForm", function() {
  const wrapper = shallowWrap(<LoginForm />);
  it("has the correct id", () => {
    expect(wrapper.is("#login-form")).to.be.true;
  });
  it("should render the username and password input", () => {
    expect(wrapper.find("Input").some('[name="username"]')).to.be.true;
    expect(wrapper.find("Input").some('[name="password"]')).to.be.true;
  });
  it("login button links to dashboard", () => {
    expect(wrapper.find("PrimaryBtn").some("[to='/dashboard']")).to.be.true;
  });
  it("should render the request password reset button", () => {
    expect(wrapper.find("#request-reset-btn").length).to.equal(1);
  });
  it("should not render a signup button", () => {
    expect(wrapper.find("#signup-btn").length).to.equal(0);
  });
});
