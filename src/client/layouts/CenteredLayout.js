import React from "react";
export const CenteredLayout = ({ navbar, footer, children }) => (
  <div
    style={{
      display: "flex",
      minHeight: "100vh",
      flexDirection: "column"
    }}
  >
    {navbar}

    <div
      style={{
        flex: "1"
      }}
      className="d-flex align-items-center justify-content-center"
    >
      {children}
    </div>
    <div className="opacity-6">{footer}</div>
  </div>
);
