import React from "react";

export const DashboardLayout = ({ navbar, footer, children }) => (
  <div
    style={{
      display: "flex",
      minHeight: "100vh",
      flexDirection: "column"
    }}
  >
    {navbar}

    <div
      style={{
        flex: "1"
      }}
      className="h-100"
    >
      <div className="py-3">{children}</div>
    </div>
    <div className="opacity-6">{footer}</div>
  </div>
);
