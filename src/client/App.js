import React from "react";
import { Button } from "reactstrap";
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import { LoginPage, Dashboard } from "./environment";
import { RouteLoggedInOnly, RouteRedirectWhenLoggedIn } from "./atoms";
export const App = props => {
  return (
    <BrowserRouter>
      <Switch>
        <RouteRedirectWhenLoggedIn
          redirectTo="/dashboard"
          path="/"
          exact
          component={LoginPage}
        />
        <RouteLoggedInOnly path="/dashboard">
          <Switch>
            <Route path="/dashboard" exact component={Dashboard} />
          </Switch>
        </RouteLoggedInOnly>
      </Switch>
    </BrowserRouter>
  );
};
