import { BrowserRouter } from "react-router-dom";
import { mount, shallow } from "enzyme";
import React from "react";
// Instantiate router context
//  https://github.com/airbnb/enzyme/issues/1112
const router = {
  history: new BrowserRouter().history,
  route: {
    location: {},
    match: {}
  }
};

export const mountWrap = node => {
  return mount(node, { context: { router } });
};

export const shallowWrap = node => {
  return shallow(node, { context: { router } });
};

export const Div = props => <div {...props} />;
