import React from "react";
import { Meteor } from "meteor/meteor";

import { LoginForm, PublicNavbar } from "../ecosystems";
import { CenteredLayout } from "../layouts";
export const LoginPage = props => {
  return (
    <CenteredLayout navbar={<PublicNavbar />}>
      <LoginForm />
    </CenteredLayout>
  );
};
