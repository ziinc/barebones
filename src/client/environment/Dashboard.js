import React from "react";
import { Meteor } from "meteor/meteor";

import { DashboardNavbar } from "../ecosystems";
import { DashboardLayout } from "../layouts";
export const Dashboard = props => {
  return (
    <DashboardLayout navbar={<DashboardNavbar />}>
      <div id="dashboard">dashboard content goes here </div>
    </DashboardLayout>
  );
};
