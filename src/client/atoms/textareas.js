import React from "react";

import { Input } from "reactstrap";

/**
 * @name textareas.TextareaBase
 * base textarea component.
 * @param {Boolean} allowEnter  prevents the preventReturnCharacter function from running
 * @param {Number} minHeight minimum height, with 3 sizes. sm, md, lg
 * @param {String} size size of the font, with 5 sizes. xs, sm, md, lg, xl
 * @param {String} [color="white"] the bootstrap color of the textarea
 */
export class TextareaBase extends React.Component {
  constructor(props) {
    super(props);
    this.preventReturnCharacter = this.preventReturnCharacter.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._adjustTextarea = this._adjustTextarea.bind(this);
  }

  componentDidMount() {
    this._adjustTextarea({});
  }
  preventReturnCharacter(e) {
    if (!this.props.allowEnter) {
      if (e.keyCode == 13) {
        e.preventDefault();
      }
    }
  }

  _handleChange(e) {
    if (this.props.onChange) this.props.onChange(e);
    this._adjustTextarea(e);
  }

  _adjustTextarea({ target = this.el }) {
    target.style.height = 0;
    const { minHeight } = this.props;
    const scrollHeight = target.scrollHeight;
    const baseSize = 40;
    const sizes = {
      sm: baseSize * 1.2,
      md: baseSize * 1.75,
      lg: baseSize * 3
    };
    const minPx = sizes[minHeight];
    target.style.height = `${
      minHeight && scrollHeight < minPx ? minPx : scrollHeight
    }px`;
  }

  render() {
    const {
      className,
      allowEnter,
      onChange,
      color = "light",
      size = "md",
      minHeight,
      ...rest
    } = this.props;

    const colors = {
      white: "bg-white",
      info: "bg-info",
      light: "bg-light",
      dark: "bg-dark",
      primary: "bg-primary",
      secondary: "bg-secondary",
      danger: "bg-danger",
      success: "bg-success"
    };
    const sizes = {
      xs: "0.6rem",
      sm: "0.8rem",
      md: "1rem",
      lg: "1.25rem",
      xl: "1.5rem"
    };

    const inputStyle = {
      display: "block",
      resize: "none",
      overflow: "hidden",
      fontSize: sizes[size]
    };
    return (
      <textarea
        style={inputStyle}
        {...rest}
        className={`${className} form-control ${colors[color]}`}
        placeholder={this.props.placeholder}
        type="textarea"
        onKeyDown={e => this.preventReturnCharacter(e)}
        ref={x => (this.el = x)}
        onChange={this._handleChange}
      />
    );
  }
}

/**
 * @name textareas.TextareaUnderlined
 * Textarea with underline
 * @param {*} props
 */
export const TextareaUnderlined = props => (
  <TextareaBase
    className="border-top-0 border-left-0 border-right-0"
    {...props}
  />
);

/**
 * @name textareas.TextareaBorderless
 * Borderless textarea component
 * @param {*} props
 */
export const TextareaBorderless = props => (
  <TextareaBase className="border-0" {...props} />
);
