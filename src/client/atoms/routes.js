import React from "react";
import { Meteor } from "meteor/meteor";
import { Route, Link, Redirect } from "react-router-dom";

export const RouteLoggedInOnly = ({
  children,
  component: Component,
  loginpath = "/",
  render,
  ...rest
}) => {
  const isLoggedIn = Boolean(Meteor.userId());

  if (!isLoggedIn) {
    return (
      <Route
        {...rest}
        render={props => (
          <Redirect
            to={{ pathname: loginpath, state: { from: props.location } }}
          />
        )}
      />
    );
  }

  if (render) {
    return <Route {...rest} render={props => render(props)} />;
  } else if (Component) {
    return <Route {...rest} render={props => <Component {...props} />} />;
  } else if (children) {
    return <Route {...rest}>{children}</Route>;
  }
};

export const RouteRedirectWhenLoggedIn = ({
  component: Component,
  render,
  redirectTo,
  ...rest
}) => {
  const isLoggedIn = Boolean(Meteor.userId());

  if (isLoggedIn) {
    return <Route {...rest} render={props => <Redirect to={redirectTo} />} />;
  }

  if (render) {
    return <Route {...rest} render={props => render(props)} />;
  } else if (Component) {
    return <Route {...rest} render={props => <Component {...props} />} />;
  } else if (children) {
    return <Route {...rest}>{children}</Route>;
  }
};
