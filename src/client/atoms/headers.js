import React from "react";

const BaseH1 = ({ children, style, ...rest }) => (
  <h1 style={{ ...style }} {...rest}>
    {children}
  </h1>
);
const BaseH2 = ({ children, style, ...rest }) => (
  <h2 style={{ ...style }} {...rest}>
    {children}
  </h2>
);
const BaseH3 = ({ children, style, ...rest }) => (
  <h3 style={{ ...style }} {...rest}>
    {children}
  </h3>
);

/** H1 HEADERS */
export const SmallH1 = ({ children, ...rest }) => (
  <BaseH1 style={{ fontSize: "1.2rem" }} {...rest}>
    {children}
  </BaseH1>
);
export const MediumH1 = ({ children, ...rest }) => (
  <BaseH1 style={{ fontSize: "1.6rem" }} {...rest}>
    {children}
  </BaseH1>
);
export const LargeH1 = ({ children, ...rest }) => (
  <BaseH1 style={{ fontSize: "2.5rem" }} {...rest}>
    {children}
  </BaseH1>
);

/**H2 HEADERS */
export const LargeH2 = ({ children, ...rest }) => (
  <BaseH2 style={{ fontSize: "2.5rem" }} {...rest}>
    {children}
  </BaseH2>
);
export const SmallH2 = ({ children, ...rest }) => (
  <BaseH2 style={{ fontSize: "1.2rem" }} {...rest}>
    {children}
  </BaseH2>
);
export const MediumH2 = ({ children, ...rest }) => (
  <BaseH2 style={{ fontSize: "1.6rem" }} {...rest}>
    {children}
  </BaseH2>
);

/**H2 HEADERS */
export const LargeH3 = ({ children, ...rest }) => (
  <BaseH3 style={{ fontSize: "2.5rem" }} {...rest}>
    {children}
  </BaseH3>
);
export const SmallH3 = ({ children, ...rest }) => (
  <BaseH3 style={{ fontSize: "1.2rem" }} {...rest}>
    {children}
  </BaseH3>
);
export const MediumH3 = ({ children, ...rest }) => (
  <BaseH3 style={{ fontSize: "1.6rem" }} {...rest}>
    {children}
  </BaseH3>
);
