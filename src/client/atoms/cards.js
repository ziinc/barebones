import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  Collapse,
  CardText,
  Row,
  Col
} from "reactstrap";

import { ClearBtn } from "../atoms";
import React from "react";

export const BaseCard = ({
  header,
  footer,
  children,
  color = "white", //defaults to white color
  ...rest
}) => (
  <Card className="m-2 " color={color} {...rest}>
    <CardBody className="py-2 px-3">
      <Row>
        <Col>{header}</Col>
      </Row>
      <Row>
        <Col>{children}</Col>
      </Row>
      {footer ? (
        <Row>
          <hr />
          <Col>{footer}</Col>
        </Row>
      ) : null}
    </CardBody>
  </Card>
);

export class ExpandableCard extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  render() {
    const { children, header, ...rest } = this.props;
    const headerWrapper = (
      <div
        className="d-flex"
        style={{ cursor: "pointer" }}
        onClick={this.toggle}
      >
        {header}
        <ClearBtn className="ml-auto align-self-start p-0">
          {this.state.collapse == true ? (
            <i className="fa fa-minus" aria-hidden="true" />
          ) : (
            <i className="fa fa-plus" aria-hidden="true" />
          )}
        </ClearBtn>
      </div>
    );
    return (
      <BaseCard header={headerWrapper} {...rest}>
        <Collapse isOpen={this.state.collapse}>{children}</Collapse>
      </BaseCard>
    );
  }
}
