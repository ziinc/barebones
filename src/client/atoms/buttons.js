import React from "react";

import { Button } from "reactstrap";

/** The base button used for style variations of the button */
const BaseBtn = ({ children, ...rest }) => (
  <Button {...rest}>{children}</Button>
);

/**
 * @name buttons.PrimaryBtn
 * Primary button
 * @param {*} props
 */
export const PrimaryBtn = props => <BaseBtn color="primary" {...props} />;

/**
 * @name buttons.PrimaryOutlineBtn
 * Primary outline button
 * @param {*} props
 */
export const PrimaryOutlineBtn = props => <PrimaryBtn outline {...props} />;

/**
 * @name buttons.PrimaryNoOutlineBtn
 * Primary button without outline
 * @param {*} props
 */
export const PrimaryNoOutlineBtn = props => (
  <PrimaryOutlineBtn className="border-0" {...props} />
);

/**
 * @name buttons.SecondaryBtn
 * Secondary button
 * @param {*} props
 */
export const SecondaryBtn = props => <BaseBtn color="secondary" {...props} />;

/**
 * @name buttons.SecondaryOutlineBtn
 * Secondary button with outline
 * @param {*} props
 */
export const SecondaryOutlineBtn = props => <SecondaryBtn outline {...props} />;

/**
 * @name buttons.SecondaryNoOutlineBtn
 * Secondary button without outline
 * @param {*} props
 */
export const SecondaryNoOutlineBtn = props => (
  <SecondaryOutlineBtn className="border-0" {...props} />
);

/**
 * @name buttons.DangerBtn
 * Danger button
 * @param {*} props
 */
export const DangerBtn = props => <BaseBtn color="danger" {...props} />;

/**
 * @name buttons.DangerOutlineBtn
 * Danger button with outline
 * @param {*} props
 */
export const DangerOutlineBtn = props => <DangerBtn outline {...props} />;

/**
 * @name buttons.ClearBtn
 * Clear link-only button
 * @param {*} props
 */
export const ClearBtn = props => (
  <BaseBtn
    style={{
      color: "none",
      textDecoration: "none"
    }}
    color="link"
    {...props}
  />
);
export const LinkBtn = props => <BaseBtn color="link" {...props} />;

export const SuccessBtn = props => <BaseBtn color="success" {...props} />;
