import React from "react";
import { Button as Btn } from "reactstrap";

/** The Button file*/

export const Button = ({ children, ...rest }) => (
  <Btn {...rest}>{children}</Btn>
);
