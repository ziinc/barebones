import React from "react";
import { expect } from "chai";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { spy } from "sinon";
import { fetchUserDetails } from "./accounts-fetchers.js";
import { mountWrap, shallowWrap } from "../helpers/test-helpers.test.js";
Enzyme.configure({ adapter: new Adapter() });

describe("fetchers/accounts-fetchers", function() {
  const Component = fetchUserDetails(props => <div {...props} />);
  const wrapper = shallowWrap(<Component />);

  it("passes correct props to wrapped component", () => {
    const hocProps = wrapper.props();
    const propKeys = Object.keys(hocProps);
    expect(propKeys).to.include.members(["username"]);
  });
});
