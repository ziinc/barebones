import React from "react";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";
/**
 * Follows
 * @param {nodes} children react nodes to pass the data to. Becomes available in the component as Children
 */
export const fetchUserDetails = withTracker(props => {
  return {
    username: undefined
  };
});
